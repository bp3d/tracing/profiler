// Copyright (c) 2022, BlockProject 3D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 3D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use druid::{commands, Data, Env, LocalizedString, Menu, MenuItem, SysMods, WindowId};
use crate::command::{DISCONNECT, DISCOVER_START, NEW, SPAWN_WINDOW};
use crate::constants::APP_NAME;
use crate::state::State;
use crate::window::{AboutWindow, PreferencesWindow};

// Due to druid inability to load lang resources packed in the app, menus have to be
// generated by hand.
// This is done this way as BP3D FORBIDS branding by libraries other than BP3D itself!

fn about<T: Data>() -> MenuItem<T> {
    MenuItem::new(String::from("About ") + APP_NAME)
        .command(SPAWN_WINDOW.with(Box::new(AboutWindow)))
}

fn preferences<T: Data>() -> MenuItem<T> {
    MenuItem::new("Preferences")
        .command(SPAWN_WINDOW.with(Box::new(PreferencesWindow)))
        .hotkey(SysMods::Cmd, ",")
}

fn hide<T: Data>() -> MenuItem<T> {
    MenuItem::new(String::from("Hide ") + APP_NAME)
        .command(commands::HIDE_APPLICATION)
        .hotkey(SysMods::Cmd, "h")
}

fn hide_others<T: Data>() -> MenuItem<T> {
    MenuItem::new("Hide Others")
        .command(commands::HIDE_OTHERS)
        .hotkey(SysMods::AltCmd, "h")
}

fn quit<T: Data>() -> MenuItem<T> {
    MenuItem::new(String::from("Quit ") + APP_NAME)
        .command(commands::QUIT_APP)
        .hotkey(SysMods::Cmd, "q")
}

fn build_app_menu() -> Menu<State> {
    Menu::new(APP_NAME)
        .entry(about())
        .separator()
        .entry(preferences())
        .separator()
        .entry(hide())
        .entry(hide_others())
        .separator()
        .entry(quit())
}

fn build_connection_menu() -> Menu<State> {
    Menu::new("Connection")
        //Closes the current connection.
        .entry(MenuItem::new("Close").command(DISCONNECT))
        //Recreates the main connection window.
        .entry(MenuItem::new("New").command(NEW))
        .entry(MenuItem::new("Auto discover").command(DISCOVER_START))
}

pub fn build_basic_menu(_: Option<WindowId>, _: &State, _: &Env) -> Menu<State> {
    Menu::new(LocalizedString::new(""))
        .entry(build_app_menu())
}

pub fn build_main_menu(_: Option<WindowId>, _: &State, _: &Env) -> Menu<State> {
    Menu::new(LocalizedString::new(""))
        .entry(build_app_menu())
        .entry(build_connection_menu())
}
